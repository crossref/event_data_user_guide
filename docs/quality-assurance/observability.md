# Observability

Event Data has a large number of moving parts: data sources, event processing, storage, and the public API. To ensure that they continue to run, we have a number of checks in place. The majority of these are also publicly accessible. To check some recent data, such as numbers of events being collected and the total available from the API, you can also use the [Jupyter notebooks for Event Data](https://gitlab.com/crossref/event_data_notebooks).

## Heartbeats

We operate heartbeats endpoints to check whether services are currently running:

 - [Query API heartbeat](http://api.eventdata.crossref.org/heartbeat/recent?since-ms-ago=1800000): returns how many events have been added to the query API recently.
 - [Quality heartbeat](http://quality-heartbeat.eventdata.crossref.org/heartbeat): checks that evidence logs are being collected and that the Twitter compliance scan is running.
 - [Agents heartbeat](http://agents-heartbeat.eventdata.crossref.org/heartbeat): checks that data is being collected by the agent and passed on to the percolator. 
 - [Event bus heartbeat](http://bus.eventdata.crossref.org/heartbeat): reports uptime of the event bus, the central repository for events. 

Uptime for the Event Data API is reported on Crossref's [status page](https://status.crossref.org).
