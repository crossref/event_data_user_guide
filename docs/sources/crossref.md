# Crossref metadata

| | |
|---------------------------|-|
| Agent Source token        | `8676e950-8ac5-4074-8ac3-c0a18ada7e99` |
| Consumes Artifacts        | none |
| Subject coverage          | All Crossref DOIs |
| Object coverage           | All DataCite DOIs |
| Data contributor          | Crossref |
| Data origin               | Crossref metadata deposited by Crossref members |
| Freshness                 | Every few hours |
| Identifies                | DOIs |
| License                   | Made available without restriction |
| Looks in                  | References in Crossref schema |
| Name                      | Crossref metadata |
| Operated by               | Crossref |
| Produces Evidence Records | No |
| Produces relation types   | `references` and other types found in the Crossref schema |
| Source ID                 | `crossref` |
| Updates or deletions      | None expected |

### What it is

Relationships from Crossref DOIs to DOIs from other registration agencies (including DataCite) and Crossref metadata records of type `dataset`. The data is supplied by Crossref members.

### What it does

Where members include references to datasets and DOIs from registration agencies other tha Crossref, the Crossref Event Data agent creates an event.

### Where data comes from

Metadata registered by Crossref members. The data is retrieved from the Crossref REST API. Both the `reference` and `relation` fields are checked.

### Example Event

    {
      "obj_id":"https://doi.org/10.13127/ITACA/2.1",
      "occurred_at":"2016-08-19T20:30:00Z",
      "subj_id":"https://doi.org/10.1007/S10518-016-9982-8",
      "total":1,
      "id":"8676e950-8ac5-4074-8ac3-c0a18ada7e99",
      "message_action":"create",
      "source_id":"crossref",
      "timestamp":"2016-08-19T22:14:33Z",
      "terms": "https://doi.org/10.13003/CED-terms-of-use",
      "relation_type_id":"cites"
    }

### Methodology

1. A member makes an XML deposit with Crossref containing references or relationships.
2. The Agent monitors new deposits (or back-fills).
3. When a link is found an Event is created if:
    - it is to a non-Crossref DOI
    - it is to a Crossref DOI record of type `dataset`
    - it has not already been reported to Event Data

### Evidence Record

No evidence record is created because the events come straight from the data source into Event Data.

### Edits / deletion

We do not anticipate that Events are ever deleted or edited.

### Quirks

DOIs must be included in the reference (in a structured or unstructured form). 

### Failure modes

 - Member may remove references before the Agent first scans the data, in which case we will not create a new Event
 - Member may deposit incorrect metadata.

### Further information

 - [Crossref Schema documentation](https://www.crossref.org/education/content-registration/crossrefs-metadata-deposit-schema/)

