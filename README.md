# Event Data User Guide 

The guide is available to read at at http://www.eventdata.crossref.org/guide

To build:

    mkdocs build

To upload:

    aws s3 sync site s3://event-data-www/guide/

Note that site uses cloudfront so there will be a delay while edge caches update. To force an emmediate update invalidate the objects you want to update in the cloudfront distribution. 

## CI

Config for CI automation:

 - S3_BUCKET - bucket including path e.g. `event-data-www/guide`
 - AWS_ACCESS_KEY_ID
 - AWS_SECRET_ACCESS_KEY
 - DIST_ID - cloudfront distribution

## License

Copyright © Crossref

Distributed under the The MIT License (MIT).


